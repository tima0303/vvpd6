# Про оттенки серого

Программа определяет, является ли цвет серым, и в случае отрицания рассматривает, сколько цвета в палитре нужно добавить, чтобы получить серый цвет.

![image-1.png](./image-1.png)

## Основные функции

Функция is_gray рассматривается, является ли комбинация RGB серым цветом

```
def is_gray(hex: str):
    """
    Функция рассматривает, является ли комбинация RGB серым цветом

            Параметры:
                    hex (str): комбинация цвета в виде RGB

            Возвращаемое значение:
                    True or False (bool): True в случае, если это комбинация равна серому цвету, False - если обратно

            Ошибки:
                    TypeError

    >>> is_gray(565657)
    Traceback (most recent call last):
    TypeError: 'int' object is not subscriptable

    >>> is_gray('#565657')
    False

    >>> is_gray('#252525')
    True
    """
    gray = '#' + hex[1:3]*3
    if hex == gray:
        return True
    else:
        return False
```

Функция to_gray рассматривает, сколько нужно добавить цвета в палитру для достижения серого цвета

```
def to_gray(hex: str):
    """
    Функция рассматривает, сколько нужно добавить цвета в палитру для достижения серого цвета

            Параметры:
                    hex (str): комбинация цвета в виде RGB

            Возвращаемое значение:
                    colour_dop (tuple): список, сколько цвета нужно добавить для достижения серого цвета

            Ошибки:
                    TypeError

    >>> to_gray(565657)
    Traceback (most recent call last):
    TypeError: 'int' object is not subscriptable

    >>> to_gray('#565657')
    (1, 1, 0)

    >>> to_gray('#252525')
    (0, 0, 0)

    """
    colour = [int(hex[1:3], 16), int(hex[3:5], 16), int(hex[5:], 16)]
    colour_dop = tuple([int(max(colour))-x for x in colour])
    return colour_dop
```
